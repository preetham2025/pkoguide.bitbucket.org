Mentor and Disciple
===================
 A perfect system for both veteran and newbie! High-level pirates can obtain reputation points through this system, while low-level ones could get good assistance.
 
###Requirements:

1) Disciples must be Level 40 or lower
2) Mentor must be Level 41 or higher
3) One disciples can have only one mentor
4) One mentor can have up to 4 disciples
 
###Awards:
1) System will give 600 reputation points to the mentor when the disciple reaches level 41
2) Disciple gets 300 reputation points, 20,000 coins, 49 Fairy Coins when they graduate at level 41
3) Before reaching Level 41, disciple also gets one item for Exp increase when they gain 1 level

 
**How to find a mentor:**
1) When you find a good guy and you want him/her to become your mentor, you could right click on him/her:

![enter image description here](http://puu.sh/sZAT9/2f56900f8d.jpg)
 
2) After him/her click on "Confirm", congratulations! Your mentor & disciple relationship is established!

 ![enter image description here](http://puu.sh/sZATw/4ca363ef12.jpg)
 
**How to accept a disciple:**
1) When you find a newbie and you want to accepct him/her as a disciple, you could right click on him/her:

![enter image description here](http://puu.sh/sZAUo/7c2b2e5aa5.jpg)


2) After him/her click on "Confirm", congratulations! Your mentor & disciple relationship is established!

 ![enter image description here](http://puu.sh/sZAVi/9afa0954ea.jpg)
 
Mentor & Disciple Chatting Channels:
     
 
![enter image description here](http://puu.sh/sZAX2/fc9b62fc5a.jpg)

![enter image description here](http://puu.sh/sZAY1/9993831f63.jpg)