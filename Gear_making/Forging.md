Equipment Forging
===================
**Why forge**

1. Forging will make your equipment stronger and better, putting different gems on different equipments makes your character stats boost;
2. Forging will make your weapon shining brightly and looks awesome.
**How to forge equipment**

1. First of all you need the basic materials required for forging, Gem, Equipment with socket, Refining Gem and of course 100,000 coins for your first forges;
2. After having your basic materials go to Shaitan Blacksmith at (900, 3495);
3. Click on "Forge" then put the equipment first, then the gem and the refining gem then pay the 100,000;
4. Then there you got it, you completely have successfully made a glowing gem on your weapon.

![enter image description here](http://puu.sh/sZNqm/1f52940e35.jpg)

![enter image description here](http://puu.sh/sZNr6/cfbec26ce7.jpg)

**Reminders**

1. First of all when forging you need to do it step by step, for example, if you get a level 3 gem and a level 3 refining gem you cannot just simply forge it to the equipment. In order to forge level 3 gem to an equipment you need to forge level 1 gem and level 1 refining gem into your equipment then after then get a level 2 gem and level 2 refining gem to upgrade it to level 2, then after your gem is at level 2 you can now forge the level 3 gem and level 3 refining gem into your equip to make it stronger.
2. When you get 2 sockets you can simply forge two different gems on each socket but the paying price costs 100,000 more on every successful forge you made. For example, you got 2 sockets, one is forged with level one gem of rage and other is forged with black dragon gem you can upgrade any of those gems but the difference is that you only pay 300,000 on it since it’s a +2 weapon and will be +3 after you forge then that will cost 300,000.
3. Example: an equipment got 2 sockets and is forged with 1 black dragon gem and the other is gem of rage, in order to forge any of it you need level 2 refining gem and level 2 black dragon gem/gem of rage in order to forge successfully. At first it will be Sword of Azure Flame +2 (+2 Because there are two level 1 gem forged on it) once you successfully forge another gem on it it’ll be called Sword of Azure Flame +3 (Because level 1 gem + level 2 gem makes the weapon +3).
4. When forging there is a chance when forging a gem higher than level 3 will fail, but some people are confuse when they thought when they are on +3 equip they have a chance of failing when forging. Don't worry, you will only get the chance of failing when your gem is level 3 and going to level 4. Even your equip is already at +5 as long as they are level 3 and level 2 gems the level 2 gem has a 100% success rate still when forging it to level 3 to make it to +6.
5. When using refining catalyst your forging successful rate will be increased for 1 minute. 
6. Of course when you fail nothing to panic the only things that will be lost are the ingredients and the money. Weapon remains and so as the gem attribute.

![enter image description here](http://puu.sh/sZNv8/4872839599.jpg) | ![enter image description here](http://puu.sh/sZNvR/3c8e3d2b28.jpg) | ![enter image description here](http://puu.sh/sZNww/b3d981cac0.jpg)