Hairstyle Guide
===================

Do you want to become cooler? Well, if so, we strongly recommend you to change your hairstyle! ^^
 After getting the required items, you could find the Hairstylist to do the hairdo for you, and you could change your hair color as well!
 
 ![enter image description here](http://puu.sh/sZJ4m/4b245130b8.jpg)

Also, many times there are some hair detailes/ trinkets that will change color too when you dye your hair (and not necessarily to the same color as your dyes). Headbands for example:

![enter image description here](http://puu.sh/sZJ5x/0bd02ad58b.jpg)

Still wanna enjoy more? No problem! If you have a Trendy Hairstyle Book (which is available in Item Mall or in-game market):

![enter image description here](http://puu.sh/sZJ6x/dcb90e1d66.png)

...you could get a brand-new cool look!

![enter image description here](http://puu.sh/sZJ7E/311367ed66.jpg)

Oh, one more thing, please notice that when you ask the Hairstylist to change your hairstyle, there is a small chance that it will FAIL. When that happens, your hair will turn RAINBOW...
Some pirates say it's horrible, while others say it's cool, what do you think? ^^
To learn more on the detailed procedures and what items you need, you could consult "Just do it!" and "New looks!"

###Just do it!

It is easy and fun to change hairstyle! You could learn it by reading the example below:
This is the original look:

![enter image description here](http://puu.sh/sZJd1/d587b40f51.jpg)
 
After getting all the required items (to know what the items are, read "New Looks"):

![enter image description here](http://puu.sh/sZJdS/5357b9d534.png)
 
Go find Hairstylist Cartel at (2215, 2813), Argent:

![enter image description here](http://puu.sh/sZJen/c9813c1d0a.jpg) 
 
Click on "I want to change my hairstyle":

![enter image description here](http://puu.sh/sZJeO/7fbdaceceb.png)
 
Then "Yes, I am sure":

![enter image description here](http://puu.sh/sZJfY/b0805060ce.png)
 
Make choices by clicking on the arrows, if the item icon is in red, it means you don't have it:

![enter image description here](http://puu.sh/sZJgV/206f01ea63.png)
 
After making correct choices, click on "Confirm":

![enter image description here](http://puu.sh/sZJhG/23079bd461.png)
 
Then YA! it's done! ^^

![enter image description here](http://puu.sh/sZJig/82762c5610.jpg)