Characters
===================
There are 4 cute characters with several professions to develop and lots of customized options:

###Handsome Lance


![enter image description here](http://puu.sh/sZvRr/056b33d1d1.jpg)


It seems that nearly all the boys like the brave hero Lance. With so many fashionable features, this handsome boy is definitely the idol in girls' eyes. By using Lance, you can choose dozens of hairstyles and outfits you like, and freely create another you in the virtual world! And you will definitely catch all the girls eyes with your conspicuous sword.

At Level 10 you can choose between many professions. Including Swordsman, Hunter and Explorer; and upon reaching Level 40, you will be provided with further choices to be a Crusader, Sharpshooter or Voyager.

###Tough Carsise

![enter image description here](http://puu.sh/sZwqB/75e694ca4d.jpg)

Brave and frank, strong and muscular, hiding his kind heart under his straightforward appearance, Carsise is the perfect choice for girls who prefer a sense of security and trust – when you're together with Carisise, he will always willingly sacrifice his life to protect you!

The vocations for Carsise are limited, but very practical: At Level 10 you can choose to become a Swordsman, and upon reaching Level 40, you will be able to become a Champion!

###Pretty Phyllis

![enter image description here](http://puu.sh/sZwH8/f745e8150b.jpg)

A mysterious and sexy lady, wherever she goes no one can ever resist giving her a second glance! The impressive expression in her eyes and the unconcerned appearance she gives is so unforgettable that everyone wonders what goes on behind those big, beautiful eyes.

There are also many vocational options for Phyllis: At Level 10 you can choose between many professions, like Hunter, Herbalist and Explore; and upon reaching Level 40, you will be provided with the further choices of Sharpshooter, Cleric, Seal Master or Voyager.

###Cute Ami

![enter image description here](http://puu.sh/sZx6L/f12c69f09a.jpg)



Lovely and cute, wherever she goes no one can ever resist giving her a second glance, too—but for different reasons :) Sometimes capricious, sometimes naughty, with the magic to activate dead atmosphere--Ami is just an elf on the continent and is the little star in everyone's eyes.

The vocations for Ami are interesting, at Level 10 you can choose between Herbalist, or Explorer; while at Level 40, you will be provided with further choices of Cleric, Seal Master, or Voyager.o hunt down these monsters and keep the citizens safe.