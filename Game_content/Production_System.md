Production System
===================
**Prerequisites for New Skills**
If you want to level new life skills, you need to master the prerequisite skills first.
EX: To learn Level 4 crafting you'll need:Level 3 crafting, Level 4 mining.
 
**Related prerequisite skills:**
Lv 1 Analyze : Level 1 Salvage
Lv 1 Crafting : Level 1 Mining
Lv 1 Manufacturing : Level 1 woodcutting
Lv 1 Cooking : Level 1 Fishing
 
**Fairy Skills**
To realize production, your fairy will need to master the respective skills as well, you could buy the skill books for them from NPCs in game.
 
Fairy skills splits into Novice, Standard and Expert. It is possible to learn standard without going through novice first.
 
**Blueprints**
Blueprints are also required for different kinds production:
![enter image description here](https://pirateking.online/forum/data/database/icon/n1419.png) **Blurry Blueprint**: 100% opens up Level 1 blueprints
![enter image description here](https://pirateking.online/forum/data/database/icon/n1691.png) **Encrypted Blueprint**: 70% opens up Level 2 blueprints. 25% opens up Level 3 blueprints. 5% opens    up Level 4 blueprints.
![enter image description here](https://pirateking.online/forum/data/database/icon/n1692.png) **Sealed Blueprint**: 70% opens up Level 4 blueprints. 25% opens up Level 5 blueprints. 5% opens up Level 6 blueprints.
![enter image description here](https://pirateking.online/forum/data/database/icon/n1693.png) **Invocation Blueprint**: 70% opens up Level 6 blueprints. 25% opens up Level 7 blueprints. 5% opens up Level 8 blueprints.
 
The blueprints above will randomly opens up:
![enter image description here](https://pirateking.online/forum/data/database/icon/n1600.png) **Manufacturing Blueprint**
![enter image description here](https://pirateking.online/forum/data/database/icon/n1603.png) **Crafting Blueprint**
![enter image description here](https://pirateking.online/forum/data/database/icon/n1690.png) **Cooking Blueprint**
 
**Tools Needed**
Tools will be given to you once you learn Level 1 of the skill. For example, Particle Crystal will be given to you once you learn Level 1 Analyze.

 
**Gathering Materials**
Items can be gathered through various means, such as life skills (woodcutting, mining, etc), from fighting monsters, refining materials through special NPCs, fairy coins, Chaos Argent prizes, and through sea commerce as well.
 
**Materials NPCs**
You could find the NPCs below to buy some key materials:

 ![enter image description here](http://puu.sh/sZHYD/f88a5a9d3f.jpg) | ![enter image description here](http://puu.sh/sZHZd/734dcbb754.jpg) |  ![enter image description here](http://puu.sh/sZHZI/1067200da5.jpg)
-------- | ---
Argent Grocery Shop|Argent Physican |Shaitan Grocery Shop
(2231, 2729)|(2250, 2770)|(840, 3585)

 ![enter image description here](http://puu.sh/sZI2B/91492e08b5.jpg) | ![enter image description here](http://puu.sh/sZI3u/aa817c4105.jpg) |  ![enter image description here](http://puu.sh/sZI4a/2a91cc3ede.jpg)
-------- | ---
Shaitan Physican | Icicle City Grocery Shop |  Icicle City Physican 
(902, 3646) | (1356, 483) | (1352, 499)
 	 	 
![enter image description here](http://puu.sh/sZI7b/370e0c965d.jpg)| ![enter image description here](http://puu.sh/sZI9o/fe9e8fadc2.jpg) |  ![enter image description here](http://puu.sh/sZIas/7d248c5160.jpg)
-------- | ---
Icicle City Fairy Merchant | Wood Processing Merchant|Ore Processing Merchant
(1370, 587)|(2258, 2717)|(905, 3524)
 
**Production Procedures**
Following is the detailed procedures for different productions:
![enter image description here](http://puu.sh/sZIcd/9ffb4ad1bb.png)**Crafting**
 1. Find the Big Furance at (759, 1474) Thundoria:
![enter image description here](http://puu.sh/sZIdj/6fae1a9a7e.jpg)
2. Open your blueprint:
![enter image description here](http://puu.sh/sZIeh/3a6d7e949f.jpg)
3. Drag the required items into the slots, then click on "Start" and choose "Big" or "Small":
![enter image description here](http://puu.sh/sZIf5/d2d43ed271.jpg)
 
![enter image description here](http://puu.sh/sZIAR/1826e3dd75.png)**Manufacturing**
1. Find the Substance Generator at (877, 3645) Shaitan:
![enter image description here](http://puu.sh/sZIBq/47f1f89383.jpg)
2.  Open your blueprint:
![enter image description here](http://puu.sh/sZICw/0354cfd645.jpg)
3. Drag the required items into the slots, then click on "Start" and "Stop":
![enter image description here](http://puu.sh/sZIDd/a515a29d41.jpg)

![enter image description here](http://puu.sh/sZIER/15dd25b8dc.png)**Analyze**
1. Find the Substance Analyzation Tool at (1341, 565) Icicle:
![enter image description here](http://puu.sh/sZIFF/332a4edf35.jpg)
> **Note:**
>  the main function of Analyze is  to change useless items into raw materials, please be cautious when taking this action-- once an item is analyzed, it will no longer be recompounded!

![enter image description here](http://puu.sh/sZIH3/27fd1feef2.jpg)

 
![enter image description here](http://puu.sh/sZIIh/79e5b6b47b.png) **Cooking**
1. Find the Matchstick at (2064, 2735) Argent, (905, 3563) Shaitan, (1363, 563) Icicle or (762, 1518) Thundoria:
![enter image description here](http://puu.sh/sZIIU/7d14ec4dd7.jpg)
2. Open your blueprint:
![enter image description here](http://puu.sh/sZIJD/d2ced3db30.jpg)
3. Drag the required items into the slots, then click on "Start" and "Stop":
![enter image description here](http://puu.sh/sZIK7/74c2662ef4.jpg)