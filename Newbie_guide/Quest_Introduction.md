Quest Introduction
===================
Quest System of TOP is featured with abundant, interesting contents and clear intention. Pirates of all different levels can find quests that are suitable for them. They’ll never fall into endless monsters killing or PVP due to lost of guidance. There are four kinds of quests in TOP: Newbie Quest, Class Quest, Normal Quest and History Quest. When pirates are qualified to the condition of quests they can go to specific NPC to accept the quests. Every pirate can accept a maximum of ten quests one time. If the quests are over burden, pirates may abandon some quests that they don’t want to do (the abandoned quests can be accpeted again later).

### Newbie Quest (Level 1 to Level 10) 
Newbie Quest will help pirates to get familiar with the game operation and functional NPC. System will automatically give a quest to pirates who enter game for the first time. This quest will lead pirates go to the Newbie Guide. During Lv1 to Lv10 pirates can accept quests from Newbie Guide. By doing the quests pirates can know the game better.

### Class Quest (Level 10 and above) 

The beginning of Class Quest means that pirates are no longer newbies. When pirates reach Level 10 they may go to relative NPC to do their promotion quests and to decide to become a swordsman, hunter, herbalist or explorer. Choosing a class is important as pirates can learn skills and be equipped with better weapons and armor. When pirates reach level 40, they can select a specialization of the class they have already chosen. 
Random quests will be generated after pirates completed their class selection. These quests include the delivering of letter or parcel, hunting of monsters or collection of materials etc. There will be rewards of gold and experience after each completed round. Also, after completion of one whole cycle (10 rounds), pirates will be rewarded randomly with a special item, armor or weapon. There is a chance of getting rare items of your class!

### Normal Quest (Level 10 and above) 
The function of Normal Quest is to guide pirates for better development in the game. When pirates reach some level to find that the monsters nearby are no longer suitable for them, specific NPC will then guide pirates to go to other places. While doing these quests, pirates will come across monsters suitable for their level. In this way pirates may get familiar with different environment and monsters.



### History Quest (Level 10 and above) 
When pirates break up with the period of newbies they may begin their journey of exploring the fantastic world of TOP. History Quest will grant pirates to seek numerous secrets, wonders and treasures.  Upon completion, pirates will be rewarded with life experience so that they can learn skills such as mining, woodcutting, setting up of stall, salvaging, repairing and fishing.


## Try it our yourself !


###Accepting Quest
If you find an NPC with an excalmatory mark on his/her head, you could talk to him/her to accept quests, please notice that you could hold up to 10 unfinished quests at a same time.

![enter image description here](http://puu.sh/sXAa1/207ef76a25.jpg)

###Checking
To check out what quest(s) you are still carrying on, you could click on the excalmatory mark on the bottom right.

![enter image description here](http://puu.sh/sXAbf/3f25e6b76e.jpg)