Apparel Upgrade
===================


**About**

Apparel upgrade is used when you want to upgrade your weapons to 110% effectiveness, having 110% effectiveness gives you some bonus stats on the original equip. Example: You got a 100 attack sword with 100% effectiveness as it's original, once it is 110% effectiveness your sword will become 110 attack sword, that is 100+10(bonus).

**How to do**

![enter image description here](http://puu.sh/sZO1V/5b719591fc.jpg)

1. Make sure you have all the ingredients before stepping in, the basic ingredients are simple you just need the apparel piece that you want to upgrade, strengthening scroll and of course strengthening crystal;
2. Go to Shaitan Blacksmith at ( 900, 3495 ) and click on "Apparel Upgrade";
3. Place the strengthening scroll then the apparel that you are going to upgrade, strengthening crystal and then a million of coins;
4. After you successfully apparel upgrade it’ll increase the effectiveness by 2%.


**Reminders**

1. When apparel upgrading it has a chance that it might fail and lose the money;
2. Apparel upgrade costs considerably so you need to think carefully before trying this kind of upgrade. Minimum of 1m per upgrade when it’s level 10 and maximum of 2.25m for level 14-15;
3. Maximum apparel upgrade level is 15.
 

 