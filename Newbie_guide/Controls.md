Controls
===================


It is easy and convenient to control your character, and there are several shortcuts which could make your journey in game smoother. Please refer to the tables below for the basic operation and usage of hotkeys.

### Keyboard

Shortcut key | Description
-------- | ---
ESC  | Open/Close System Settings
TAB | Change Chat Channel
INSERT | Stand/Sit/Allow Character to restore HP/SP automatically 
SHIFT | Show Monsters and Players Name
ENTER | Confirm Selection/Send Message
ALT + E | Allow Character to restore HP/SP automatically
ALT + G  | Open/Close Guild Interface
ALT + T | Open/Close Shop Interface
F1 to F8 | 	Use for Skills/Items/Potions
CTRL + O to Q | Use for Emoticons
ALT + S |  Open/Close Skills Interface
ALT + Q | 	 Open/Close Quest Log
ALT + A |  	 Open/Close Character Interface
ALT + F | 	  Open/Close Friend Interface
ALT +   B | 	On/Off Stall Interface
ALT +   W | 	On/Off World Map
CTRL + LEFT CLICK | Force Attack
Print Screen | Take a Screenshot

### Mouse

Shortcut key | Description
-------- | ---
Left Click  |  Move Character / Lean on Walls / Sit on Bench /  Select Character Actions
Double Right Click | Restore Default Camera Angle
Mouse Wheel | Zoom In/Out
Hold Left Click | Show Monsters and Players Name
Right Click | Invite Party / Request Trade / Add Friend / Invite Guild / Item Description / Skill Description
Double Left Click | Use Item / Activate Skill
Hold Right Click | Rotate Camera Angle


Game interface functions and operation:

![enter image description here](http://puu.sh/sXzDj/6a5eb35378.jpg)



