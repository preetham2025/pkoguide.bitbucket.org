Character Stats
===================
Character Stats are a collection of all a character’s information in one place. This includes the Character Name, Class, Level, Base Attributes, Additional Attributes, and Fame.

![enter image description here](http://puu.sh/sZyiJ/c742b756ff.jpg)

Attributes | Description
-------- | ---
Character Name | Displays the name set when the character was created
Class|Displays the current profession
Level|	Display the current level
EXP	|The Experience required to move up to the next level as calculated by percentage. The character is able to level up when it reaches 100 %. (Exp is obtained from fighting or quests.)
STR	|Increases Melee attack power
AGI	|Increases Attack speed and Dodge rate
CON|	Increases Defense, Max HP and HP recovery rate
SPR	|Increases Max SP and SP recovery rate
ACC|	Increases Ranged attack power and Hit Rate

