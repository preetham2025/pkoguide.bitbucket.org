Socket Making
===================
Why making sockets

First of all in order to forge your weapon/armor you need a socket, so that you could put gem into it. You have two means to get socket equipment:

1. You could get it from a monster drop that has 1 (One) socket on it
    already.
2. You could make a socket on the equipment by yourself.

**How to make sockets on an equipment**
![enter image description here](http://puu.sh/sZMUG/af15b93293.jpg)
1. You need an equipment, equipment catalyst, equipment stabilizer  and 50,000 coins;
2. After having those materials, go to Shaitan Blacksmith at (900, 3495);    
3. Click on "Fusion" then put the equipment first, then the equipment catalyst and stabilizer then pay and click "ok".    

![enter image description here](http://puu.sh/sZMVE/9b909a9190.jpg)

![enter image description here](http://puu.sh/sZMWh/4f6f9582b3.jpg)

![enter image description here](http://puu.sh/sZMWX/bb68eeb207.jpg) | ![enter image description here](http://puu.sh/sZMXX/f72d2986bd.jpg)

Then congratulations! there you got it!


> **Note:**
> You can only make 2 sockets, 3 sockets items could only get from monster drops, but the chance is quite small;
> 2 (Two) sockets can be made without failure when making sockets;
> Be careful on not to put the wrong equipment because once you are done making sockets, the ingredients will be gone, too!
 
 

 

