Chaos Argent
===================
Wanna know who is the toughest pirate? Come and join the Chaos Argent!
 
Go find Chaos Administrator at (1374, 529), Icicle with your Medal of Valor and pay the fee:

![enter image description here](http://puu.sh/sZEW1/2650398882.jpg)
 
When the time comes, you'll find the entrance:

![enter image description here](http://puu.sh/sZEWm/754cced365.jpg)
 
Then welcome to the battlefield!

![enter image description here](http://puu.sh/sZEX2/b2c9787045.jpg)

 
If you're lucky enough, you'll find:

![enter image description here](http://puu.sh/sZEXY/801b51a5cc.jpg)
 
Open it up, wow! ^^

![enter image description here](http://puu.sh/sZEYL/e4d065fe83.jpg)
 
Here everyone is good, so please take care of yourself! ^^

![enter image description here](http://puu.sh/sZEZL/29a86fcbc8.jpg)
 
After battle, you could find Chaos Administrator again with your Medal of Valor to apply to become a Chaos Overlord!

![enter image description here](http://puu.sh/sZF06/3294d61a23.jpg)
 
Note: please notice that for Carsise, if pirates combine the Chaos Framestone obtained in Chaos Argent with vests, the tattoo on Carsise will disappear and pirates will not able to use some skills untill remake the tattoo.

> **Note:**
> Please notice that for Carsise, if pirates combine the Chaos Framestone obtained in Chaos Argent with vests, the tattoo on Carsise will disappear and pirates will not able to use some skills untill remake the tattoo.
