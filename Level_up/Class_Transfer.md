Class Transfer
===================
Each character has strengths and weaknesses, which determine their proficiency at certain skills. A player needs to reach Level 10 before they are eligible for their first class transfer. Level 10 can be achieved quickly by completing the beginner's quests. A player is eligible for his second class transfer at Level 40.

###First Class Transfer:


Class | City | NPC
-------- | ---
Swordsman| 	Argent City | 	Castle Guard Peter (2194.2768)
Hunter| 	Icicle Castle |  	Icicle Swordsman Ray (1365, 570)
Herbalist| 	Shaitan City|  	High Priest Gannon (862, 3500)
Explorer	| Argent City| 	Little Daniel (2193, 2730)

First class transfer character development: 

Class: **Swordsman**
Rewards: Some experience, Short sword, Concentration
Starts quests at Castle Guard Peter
Ends quests at Castle Guard Peter
Related NPCs: Newbie Guard, General William
Quest Items: Courage certificate

Missions: 
1. Slay Piglets for Castle Guard Peter.
2. Deliver a letter to General William for Castle Guard Peter.
3. Gather 3 Pure Wools for General William.
4. Bring 1 Courage Certificate to Castle Guard Peter.


Class: **Hunter**
Rewards: Some experience, Short Bow, Range Mastery 
Starts quests at Icicle Swordsman Ray
Ends quests at Icicle Swordsman Ray
Related NPCs: Little Mo (1237, 611)
Quest Items: Hunter Manual

Missions: 
1. Slay 12 Little White Deer for Icicle Swordsman Ray.
2. Deliver a letter to Little Mo for Icicle Swordsman Ray.
3. Gather 3 Medicine Bottles for Little Mo.
4. Bring 1 Hunter Manual to Icicle Swordsman Ray.


Class: **Herbalist**
Rewards: Some experience, Praised Branch, Heal
Starts quests at High Priest Gannon
Ends quests at High Priest Gannon
Related NPCs: Navy HQ Admiral Nic (865, 3648)
Quest Items: Righteous Document

Missions: 
1. Slay 2 Cactuses for High Priest•Gannon.
2. Deliver a letter to Navy HQ Admiral Nic for High Priest Gannon.
3. Gather 3 Medicated Grasses for Navy HQ Admiral Nic.
4. Bring 1 Righteous Document to High Priest Gannon.


Class: **Explorer**
Rewards: Some experience, Knife, Diligence
Starts quests at Little Daniel
Ends quests at Little Daniel
Related NPCs: General William
Quest Items: Survival Compass

Missions: 
1. Slay 12 Piglets for Little Daniel.
2. Deliver a letter to General William for Little Daniel.
3. Gather 2 Poisoned Fruits for General William.
4. Bring 1 Survival Compass to Little Daniel.

###Second Class Promotion:
Class | City | NPC
-------- | ---
Crusader |	ArgentCity|	Castle Guard Peter
Champion|	Argent city|	Castle Guard Peter
Cleric	|Shaitan city	|High Priest Gannon
Seal Master	|Shaitan city	|High Priest Gannon
Sharpshooter	|Icicle castle|	Icicle Swordsman Ray
Voyager	 | Argent city	|Little Daniel

Second class promotion character development:

Class: **Champion**
Requires Level 40+ Swordsman
Rewards: Becomes a Champion
Starts quests at Castle Guard Peter
Ends quests at Castle Guard Peter
Quest Items: Criss Sword (two-handed), 1 Ninja Mask, 5 Solid Rocks and 5 Great Bear Teeth

Missions: 
1. Gather the followings for Castle Guard•Peter.
Criss Sword (Available in the weaponry of ArgentCity)
Ninja Mask (Available from the Ninja Moles in <Abandoned Mine 2>)
Solid Rocks (Available from the Rock Golems in AndesForest Haven)
Great Bear Tooth (Available from the Polar Bears in Skeleton Haven)
2. Bring these items to Castle Guard Peter.
 

Class: **Crusade**r
Requires Level 40+ Swordsman
Rewards: Becomes a Crusader
Starts quests at Castle Guard•Peter
Ends quests at Castle Guard•Peter
Quest Items: Breast Plate, Greasy Lizard Skin and Warrior Certificate

Missions: 
1. Gather the following for Castle Guard•Peter:
Breast Plate (Available from Tailor Granny-nila in ArgentCity)
Greasy Lizard Ski (Available from the Thickskin Lizards in Ascaron)
Warrior Certificate (Available from the Skeletal Warriors in Deep Blue)
Slay 10 Skeletal Warriors (Deep Blue) for Castle Guard Peter.
2. Return to Castle Guard•Peter after completing those above.
 

Class: **Sharpshooter**
Requires Level 40+ Hunter
Rewards: Becomes a Sharpshooter
Starts quests at Icicle Swordsman Ray
Ends quests at Icicle Swordsman Ray
Quest Items: 5 Broken Ninja Swords and 5 Old Quivers

Missions: 
1. Complete the following tasks for Icicle Swordsman•Ray:
Gather Broken Ninja Swords (Available from the Ninja Moles in <Abandoned Mine 2>)
Gather Old Quivers (Available from the Skeletal Archers in Skeleton Haven)     
Slay 10 Skeletal Archers (Skeleton Haven) for Icicle Swordsman•Ray.
2. Return to Icicle Swordsman•Ray after completing the above tasks.
 

Class: **Cleric**
Requires Level 40+ Herbalist
Rewards: Becomes a Cleric
Starts quests at High Priest•Gannon
Ends quests at High Priest•Gannon
Related NPCs: Granny Dong (Icicle Haven), Doctor - Chivo (Chaldea Haven)
Quest Items: 2 Fancy Petals, 4 Panaceas and 6 Elven Fruit Juices

Missions: 
1. Help High Priest•Gannon gather some items:
Fancy Petals (Available from the Mystic Flowers, Sleepy Snails or Slowpoke Snails in ArgentCity)
Panaceas (Available from the Mud Monsters in <Mine 1>)
Elven Fruit Juices (Available from the Wolves, Cumbersome Snowmen and Needle of Stramonium in Valhalla Haven)
2. After gathering all the items, deliver them to Granny Dong and Doctor - Chivo.
3. Return to High Priest•Gannon when the above tasks are completed.
 

Class: **Seal Master**
Requires Level 40+ Herbalist
Rewards: Becomes a Seal Master
Starts quests at High Priest•Gannon
Ends quests at High Priest•Gannon
Related NPCs:  Holy Priestess - Ada
Quest Items: Dingle 
Bell, Dangerous Sharp Claw and Heart of Purity

Missions: 
1. Help High Priest•Gannon gather the following items:
Dingle Bell (Available from the Crazy Sheep in Skeleton Haven)
Dangerous Sharp Claw (Available from the Stinging Beaks in Valhalla Haven)
Heart of Purity (Available from the Snow Spirits in Atlantis Haven)
2. Deliver the items to Holy Priestess - Ada.
3. Return to High Priest•Gannon after completing the above tasks.
 

Class: **Voyager**
Requires Level 40+ Explorer
Rewards: Becomes a Voyager
Starts quests at Little Daniel
Ends quests at Little Daniel

Missions: 
1. Help Little Daniel send messages to the 4 traders of the 4 havens.
2. Return to Little Daniel in ArgentCity.