Gem Combining
===================
**Why combine**

Gem combining is needed for forging especially when you are going to upgrade your gem level. Grocer Amos at (840, 3585), Shaitan City sells Combining Scroll for pirates to combine their gems. Pirates will need 2 gems of the same type and level in order to combine. Gem combining will have a certain rate of failure when combining to level 4 or higher.

**How to combine gems and refining gems**

1. First of all you need gem, refining gem and of course the gem combining scroll which can be bought from Amos.
2. Talk to Grocer Amos at Shaitan and Select "Combine".
3. Place the combining scroll at the top slot.
4. Insert 2 attribute gem (Rage – Rage, Wind – Wind) or Refining gem of same level into the two remaining slots.
5. Each combining of gems requires 50,000 coins.

![enter image description here](http://puu.sh/sZNGT/a907ea8e97.jpg)

 

**Reminders**

1. When combining you have to have the same attribute gem and level in order to combine it right, because it won’t work if you are combining level 3 and level 5 gems with same attribute gem.
2. When your gem level is at 3 and you still want to combine it to make it level 4 there is a certain chance that your combining process will fail (when failed, the gems will disappear).So it’s better to use Composition Catalyst to increase your chances of being success. . 
3. Combining refining gems are like combining attribute gems. Same failing chance, same everything.
4. There is no max level when combining so good luck when combining it ^_^

**More examples:**
![enter image description here](http://puu.sh/sZNJ6/ad555581c2.jpg)


![enter image description here](http://puu.sh/sZNK1/24ee2076a5.jpg)

![enter image description here](http://puu.sh/sZNKL/776972a28b.jpg)

![enter image description here](http://puu.sh/sZNLG/0c056e44f1.jpg)





