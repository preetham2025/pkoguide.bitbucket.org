Fairy Marriage
===================
 ![A perfect system for both veteran and newbie! High-level pirates can obtain reputation points through this system, while low-level ones could get good assistance.](http://puu.sh/sZBYa/815f9c5059.jpg)
 

To get a Second Generation fairy, pirates only need to take some easy steps:
 
 
> **Note:**

> - Fairies should be lvl 20 and above to under go  Fairy Marriage. 
> - Only 1st generation Fairies can undergo Marriage.
> - Keep the fairies stamina above 100 before Marriage.

**Step 1**: Go to Shaitan City and find NPC Langa at (852,3549);

![enter image description here](http://puu.sh/sZC02/66f47707e7.jpg)
 
**Step 2**: Click on the NPC，Select “Conditions to conceive fairies”;

 ![enter image description here](http://puu.sh/sZC0C/2041e83f4e.jpg)
 
**Step 3**: Take a look of the requirements;

 ![enter image description here](http://puu.sh/sZC1g/d2da4d9a8d.jpg)
 
**Step 4**: Select "Fairies marriage" and move the demonic fruit and the two fairies to the frame. Make sure there are two other necessary Items which will be used to conceive the Second Generation fairies. At last, click on "Confirm";

![enter image description here](http://puu.sh/sZC1K/80e2a69954.jpg)
 
**Step 5**: Pay the fee;

![](http://puu.sh/sZC2E/765c7702c5.jpg) 

Congratulations, you now have got a cute second generation fairy and a Skill book( Either Novice, Standard or Expert Possession Skill book)!

![enter image description here](http://puu.sh/sZC3A/01a3a99a5c.jpg)

So far, the highest level for the Second Generation fairy is Lvl 41.




 Fairy | Required Items | Possession Effect
-------- | ---
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1651.png)  Fairy of Luck | Demonic Fruit of Acidity, **10** Tasty Squid Meat, **10** Sorrow Warrior Carcass | Adds drop rate bonus. Formula : Pet level * 2% of drop rate added.
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1652.png)  Fairy of Strength | Demonic Fruit of Strength, **10** Arabic Pearl Fragment, **10** Wailing Warrior Carcass| Adds Strength(STR) points depending on pet level.
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1653.png)  Fairy of Constitution | 	Demonic Fruit of Courage, **10** Cracked Arabic Pearl, **10** Sorrow Archer Carcass |Adds Constitution(CON) points depending on pet level.
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1654.png) Fairy of Spirit | Demonic Fruit of Intellect, **10** Polliwog Tail, **10** Wailing Archer Carcass |Adds Spirit(SPR) points depending on pet level.
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1655.png)  Fairy of Accuracy	 | Demonic Fruit of Energy, **10** Fish Spikes, **10** Mud Chunk | Adds Accuracy(ACC) points depending on pet level.
 ![enter image description here](https://pirateking.online/forum/data/database/icon/n1656.png)  	Fairy of Agility | Demonic Fruit of Abberant, **10** Shark Fin, **10** Swamp Wood | Adds Agility(AGI) points depending on pet level.
  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1657.png)  	Fairy of Evil | Demonic Fruit of Mystery, **10** Sparkling Arabic Pearl Fragment, **10** Mud Slice |Adds exp rate bonus. Formula : Pet level * 2% of exp rate added.



###Possession Skill Book

When Fairies undergo marriage they will produce the either one of the following skills books:

**Produced when two 1st generation fairy level is 20-24:**
85% Novice Possession
12% Standard Possession
3% Expert Possession

**Produced when two 1st generation fairy level is 25-34**
90% Standard Possession
10% Expert Possession

**Produced when both 1st generation fairy level is 35+**
100% Expert Possession

Possession is a skill that can be learnt by a character. To activate the possession skill, a second generation fairy should be equipped. Possession skill provides bonus stats, experience or drop depending on the fairy equipped. Refer the table above for information regarding Possession effect.

Difference between possession is only on the stamina used. Novice possession consumes 3x more stamina than Expert, and Standard consumes 2x more.

> **Note:**
> Possession skill only works when a second generation fairy is equipped, **will not work with a First Generation Fairy.**

Parts of the guide are information provided by LuisaRLZ.