Apparel Fusing
===================
**About**

Apparel fusing is the use of the Item Mall Decoration and is used to make your character look cooler than before and be unique. You need some certain items in order to apparel fuse and you need also money to fuse it. You'll know what items you need and what they are used for and we shall start now.

**Fusion Scroll**- a scroll which will do the job to fuse the apparel and the original equipment
**Fusion Catalyst**- a scroll which will be needed when you are fusing an equipment with a gem with it
**Apparel**- can be bought in item mall or from other pirates, is used to apparel fuse with
**Equipment**- original armor, helm, gloves, boots or weapons that pirates wish to fuse with

**How to Fuse Apparel**

![enter image description here](http://puu.sh/sZNQB/711a3070b5.jpg)

**How to do**

1. First of all you need the basic ingredients;
2. Talk to Shaitan Blacksmith at (900, 3495) and select "Apparel Fusion";
3. Place Fusion Scroll, Apparel, and Equipment and Catalyst (if you got the original equipment with gems) into the slots;
4. Then there you have it-- your new look of equipment! ^_^

![enter image description here](http://puu.sh/sZNUl/e68ca65ed5.jpg)


**Reminder**

1. If your equipment has been imbued with gems before, you will have to insert a Fusion Catalyst as well in order to retain the gems attribute. However, if your equipment has never undergone any forging process, you do not need to insert the Fusion Catalyst;
2. There is no chance of failing when you are going to fuse something;
3. Apparel fusion price is always x10 the level your going to fuse (example: Level 60 = 60k, Level 50 = 50k, Level 40 = 40k);
4. After Apparel Fusion process, original [Equipment] will disappear and replaced with [Apparel] which has the attribute of the original [Equipment].
5. Once you successfully apparel fuse the item will be at 100% effectiveness.
> **Note:**
> If you fuse the fused apparel into a new one, according to the apparel fusion rule, the effect will be set to 100%