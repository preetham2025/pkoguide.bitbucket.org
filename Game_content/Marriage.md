Marriage System
===================
> **Note:**
> This feature is not yet implemented in PKO.

Want to get married to your lover and help each other through the good times and bad... together? Well, take a look at what will be available very soon!

**Marriage Registration**

1. Go find the NPC priest in **Argent City(2185, 2786)**.

2. Lovers who want to get married must be in a team together. 

3. The lovers must wear their wedding outfits. 

4. Both lovers must have 10million Gold and have already purchased the Valentine's Day Ring from the Item Mall.

If you meet these requirements, you can register for marriage with the Priest and obtain a Marriage Certificate. Players must go find the Church NPC Maylada to bid on the church, purchase the church tickets and church invitation cards, and visit the church at a specified time.


**Church Visit**

**NPC**: Maylada in Shaitan City (822,3534).

**The tickets**: Can be bought from Maylada, 5000 Gold per ticket.

Time to visit: From 5:00pm-5:10pm EST (GMT-5) on Monday. After time is up, the players will be teleported to Shaitan City automatically.


**Church Bidding**

**Rules:** Hold a Church Bid Card to bid according to the reserve price at corresponding time. 

The winner will obtain 1 Church Permission booklet (with the time listed), 10 Guest. 

Treasure Chest (which may contain the church invitation listing the time) and their Church Bid Card will be deducted.


**Bidding Times**

Bids for the 1st Church time slot on Saturday are Every Monday 6:00 pm-6:30 pm EST (GMT-5)

Bids for the 2nd Church time slot on Saturday are Every Monday 6:40 pm-7:10 pm EST (GMT-5)

Bids for the 1st Church time slot on Sunday are Every Monday 7:20 pm-7:50 pm EST (GMT-5) 

Bids for the 2nd Church time slot on Sunday are Every Monday 8:00 pm-8:30 pm EST (GMT-5)


**Church Celebration**

1. 1st time slot on Saturday and Sunday: 6:00 pm-8:00 pm EST (GMT-5) 

2. 2nd time slot on Saturday and Sunday: 8:10 pm-10:10 pm EST (GMT-5) 

3. Players must bring their Church Permission booklet or other related Celebration Invitation Cards with them when they enter the church.

4. Two hours later, players will be teleported to Shaitan City automatically.


**Wedding Ceremony**

1. Enter the church within the specified time. 

2. The lovers must wear their wedding outfits. 

3. Make sure to take the Marriage Certificate and the Valentine's Day Ring with you. 

4. Lovers who want to get married must form a two-person team. 

5. Chat with the NPC Magician Chiatan to start the celebration.


**About the Props** 

1. Valentine's Day Ring: A necessary item in the marriage system. The bride and groom can go find the NPC Magician Chiatan in the church to bind the rings and learn the Love line ability. This special ability can be used to find each other on a specified map if both of them have their rings and are in a lovers team.
**To use the Love Line ability players need to make sure they,**
a. Have their Love Rings with them.
b. Have formed an exclusive team.
c. Are on the same map. This ability can only be used in Argent City, Shaitan City, Icicle City, the Demonic World and the Sacred War Map.
2. Celebration Invitation Card:
Can be opened from the Guest Treasure Chest. A guest who wants to enter the church must show their Celebration Invitation Card. The card must list the correct time. Double click to enter the church. The card can only be used at the time listed and must be sent by the church user.

![enter image description here](http://puu.sh/sZLOy/d5b3e7b02d.jpg) 

![enter image description here](http://puu.sh/sZLRy/8355427e49.jpg)