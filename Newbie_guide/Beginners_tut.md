Beginners Tutorial
===================

If you have entered the community successfully but have no idea on what to do first, don't worry, here's a simple guide teaching you how to start your life.

![enter image description here](http://puu.sh/sXxiZ/6da3e27b00.jpg)

> **Note:**
Please notice that you're not able to voice out through World Channel before reaching Level 10.

![enter image description here](http://puu.sh/sXxl6/15dee01572.jpg)

Click “Inventory”, drag the armors and weapons up, and see what you look like now ^^

![enter image description here](http://puu.sh/sXxmZ/5da6fa6739.jpg)

> **Note:**
A small tip--some items (and in the future, all initiative skills) can be dragged into the shortcut panel, which sometimes can be very convenient.

![enter image description here](http://puu.sh/sXxpL/75119153d8.jpg)


Then click “stats”, as a beginner, you'll have 5 points to distribute freely (later more). Move your mouse onto the “+”, you'll find the relevant introductions.

![enter image description here](http://puu.sh/sXxrd/5e35994153.jpg)

Here and there, you'll find lots of players; you can use the “Friend” in game and invite them to become your friends.

![enter image description here](http://puu.sh/sXxtp/fe7b5fc016.jpg)

Put your mouse on the player you want to invite, right click and hold, you'll find the option. (You could find other options such as Trade and Party Invite which are very useful and interesting as well, please try them out by yourself later ^^) 

![enter image description here](http://puu.sh/sXxxi/4b022a1924.jpg)

If you want to talk with more than one friend at a same time, simply drag the other friend(s) in! Isn't this cool? ^^

![enter image description here](http://puu.sh/sXxC2/99a48c155e.jpg)

OK, time to mind the real business—find the Newbie Guide!

![enter image description here](http://puu.sh/sXxGR/67b7dd8bd8.jpg)


You can find Newbie Guide as well as other NPCs (NPCs refer to non-player characters) and monsters through relevant sections on the official website.
“OK, I know where the Newbie Guide is and also have her coordinate now, but wait, how can I check out the way to her?”
Don't worry, thanks to the developer, they've built clever radar for every one of us!
On the right top you can find your coordinate under the window, click the arrow:

![enter image description here](http://puu.sh/sXxMC/c0d24cfb1c.jpg)

Enter the coordinate you want to get to:

![enter image description here](http://puu.sh/sXxNr/9454a7ccc5.jpg)

You'll find a yellow arrow which will lead you to the exact place you want to go!

![enter image description here](http://puu.sh/sXxOl/7fdf400ed7.jpg)

OK, no problem anymore, find the Newbie Guide, she'll teach you all what you have to do at the very beginning!

Of course, this is only just a very simple guide for very beginners, in the future, with more practice, more visiting of the website, more experience sharing with friends in game, you'll certainly learn much more useful and interesting things such as skills, sailing, guild, etc.

And as long as you have ambition, you're sure to become a TOP-famous pirate finally!

Good luck and have fun!