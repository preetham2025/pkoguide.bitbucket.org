More Knowledge&Tips
===================
###Selling and Buying
You could sell items to or buy them from many NPCs, for example:

![enter image description here](http://puu.sh/sXAfI/de42719782.png)

Choose "Trade":

![enter image description here](http://puu.sh/sXAgH/7f1dc2c7a4.png)

Then drag the items you want to buy or sell, you'll be asked to accept or pay different amount of money.

![enter image description here](http://puu.sh/sXAhX/e19213a343.png)

###Teleportation
You could teleport easily from one place to another, just find the Teleporters in different places:
![enter image description here](http://puu.sh/sXArW/c601aa7954.jpg)

Talk to them and choose the place:

![enter image description here](http://puu.sh/sXAwN/bc2724fea2.png)

> **Note:**
1. You could also record your spawn point, it means next time when you're "dead", you will revive at the place that you have recorded;
2. If you teleport from one place to another, you will be asked to pay certain amount of fee, and record spawn point is free.


Banking
If you have got too many items with you, you could find the Bankers in different cities to save some of them:
![enter image description here](http://puu.sh/sXABJ/52a3e81ca7.jpg)

And the fee will be 200 coins for every transaction:
![enter image description here](http://puu.sh/sXADd/9478b7d402.png)

## Tips

**Shift+Ctrl+A**
After defeating monsters, this is a group of very useful hotkeys to pick up dropped items.

**How to protect account?**
1.	Use a long password. The longer your password is, the harder it is for hacker to obtain it;
2.	Never let anyone know your account password;
3.	Don't share or trade your account with anyone, it is both prohibited and unsafe. If you lost your account because of that, you must take full responsibility for the consequence of it;
4.	Don't trust anyone who offers free item mall points or items. Once you believe it, they will ask you for your Passport ID and password, and then you may be hacked;.
5.	Beware of fraud sites, fake GMs and bots.


**How to seek for help?**
1.	Post your problem at Support Center on Pirate king online forum.
2.	Use Forum for reading community guides and to seek help from the community.