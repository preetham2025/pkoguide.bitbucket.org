Fairy Pets
===================

----------

[TOC]


----------

Introduction
-------------
Fairy pets are adorable, royal and helpful; they're their masters' best assistants! So far in Pirate King Online there are all together there are two generation of fairies.

First Generation Fairies

id   | Fairy | Description 
-------- | ---
183 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1336.png)  Fairy of life | Symbol of light
184 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1337.png)  Fairy of Darkness | Symbol of Vengeance
185 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1338.png)  Fairy of Virtue | 	Symbol of Humility
186 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1339.png)  Fairy of Kudos | Symbol of Honor
187 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1340.png)  Fairy of Faith | Symbol of Honor
188 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1341.png)  Fairy of Valor	 | Symbol of Courage
189 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1342.png)  Fairy of Hope | 	Symbol of Honesty 
200 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1343.png)  Fairy of Woe | Symbol of Remorse
201 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1344.png)  	Fairy of Love	 | Symbol of Innocence
202 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1345.png)  Fairy of Heart | 	Symbol of Trust

Second Generation Fairies

id   | Fairy | Description 
-------- | ---
231 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1651.png)  Fairy of Luck | New generation of fairy that was conceive from a fairy's marriage
232 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1652.png)  Fairy of Strength | New generation of fairy that was conceive from a fairy's marriage
233 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1653.png)  Fairy of Constitution | 	New generation of fairy that was conceive from a fairy's marriage
234 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1654.png) Fairy of Spirit | New generation of fairy that was conceive from a fairy's marriageSymbol of Honor
235 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1655.png)  Fairy of Accuracy	 | New generation of fairy that was conceive from a fairy's marriage
236 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1656.png)  	Fairy of Agility | New generation of fairy that was conceive from a fairy's marriage
237 |  ![enter image description here](https://pirateking.online/forum/data/database/icon/n1657.png)  	Fairy of Evil | New generation of fairy that was conceive from a fairy's marriage


> **Note:**
> - 2nd generation fairies are obtained by marrying two 1st generation fairies.
> - More information can be obtained from " Pet Marriage Section "

####  Abilities
Like their masters, fairy pets also have abilities, and the abilities of a fairy pet can be divided into 4 different categories: Basic Attributes, Stamina, Growth and Skills.

Abilities    | Description
-------- | ---
Basic Attributes | Each fairy pet has 5 attributes, namely Strength, Accuracy, Agility, Spirit and Constitution. These attributes can be cultivated by feeding the fairy pet with different energy food. And when the fairy is equipped, its attributes will be added into its masters'.
Stamina    | The Stamina bar (SP) of a fairy pet is like the health bar (HP) of a character. The fairy pet can only function normally if its Stamina bar is above 0.
Growth     | 	Used to judge how much your fairy pet has grown. You can feed it with different energy food when its growth bar (EXP) is at max.
Skills     |   A fairy pet can possess up to 3 skills to assist its master in battle.

#### Acquisition 
The fairies could be obtained through several means, namely:

- Story quest
- Monster Drops
- In game Item Mall by Purchasing Rums. 
- In game Market

----------


User Operation
-------------------

#### Equipping 
To equip your fairy, simply drag it into the second slot of your Inventory.

![enter image description here](http://top.igg.com/pet/images/1.gif)

Then you could find your little partner flying around you with its status bar under your character status bar.

![enter image description here](http://top.igg.com/pet/images/2.gif)

#### Raising 
When your fairy is hungry (SP is not full), You could feed it with pet food or rations.

![enter image description here](http://top.igg.com/pet/images/3.gif)

To feed your fairy: double click the pet food or ration, a chicken leg will show up, move it onto your fairy, then click.

![enter image description here](http://top.igg.com/pet/images/4.gif)


> **Note:**

> - You can feed you pet with Auto ration which feeds the pet automatically after stamina is below 50.
> - Auto rations are available in Item Mall. 

####  Leveling up

You could level up your fairies with energy food. The operation is similar to Raising.

![enter image description here](http://top.igg.com/pet/images/6.gif)


####  Teaching 
You could teach your fairies with Skill books. The operation is similar to Raising.

![enter image description here](http://top.igg.com/pet/images/8.gif)



----------


Leveling Up
-------------

 	
Like their masters, fairy pets could level up as well.

**Growing up**
Fairy will grow up and stamina points will be consumed as long as it is equipped, and its Growth Bar will increase. When the stamina reaches 0, fairy will be disabled (it will not die, just exhausted-- could not grow up and function normally). Usage of the fairies' skills will also take up some stamina points. To restore the stamina of the fairy, you can feed it with the food or rations available in game and Item Mall.

**Attributes Increase**
The growth bar of a fairy pet will increase with time, when its master is online and it is equipped. When the growth bar reaches maximum, you can feed it with different energy food to develop its attributes, and it will gain one point in one attribute and increase one level if the feeding succeeds. But there is a certain rate of failure. Success rate becomes lower as your pet becomes stronger and higher on level. If succeeded, the whole growth bar will be depleted. If failed, half of the growth bar will be depleted.

**Energy Food**
There are all together five types of energy food to develop a fairy's five attributes:



Energy Food | Description
-------- | ---
![enter image description here](https://pirateking.online/forum/data/database/icon/n1346.png)Snow Dragon Fruit | Increases the Strength
![enter image description here](https://pirateking.online/forum/data/database/icon/n1347.png)Icespire Plum | Increases the agility
![enter image description here](https://pirateking.online/forum/data/database/icon/n1348.png)Zephyr Fish Floss  | Increases the accuracy
![enter image description here](https://pirateking.online/forum/data/database/icon/n1349.png)Argent Mango | Increases the constitution
![enter image description here](https://pirateking.online/forum/data/database/icon/n1350.png)Shaitan Biscuit  | Increases the spirit

> **Note:**
> -  So far, the highest level for a fairy is Lvl 41.
> -  Alternatively , you can use Great pet fruits increase pets level by 2.


----------


Skills Learning
---------------------

Like their masters, fairies also have skills, which they use automatically in game; and sometimes, it could be very useful. 

**How to Learn** 
Your pet will be able to learn skills from different scrolls and skill books available in game or Item Mall. A pet fairy can have up to 3 skills. When a pet fairy uses a new scroll or skill book, there will be a certain chance that the new skill will overwrite one of the existing skills it possesses.

**Skill Types**
There are 5 different types of skills and each of them has got three grades, namely Novice, Standard and Expert. A skill of higher grade will overwrite the same skill of a lower grade.

Skills | Description
-------- | ---
Protection | When the master' HP falls below 20%, it will absorb all normal damage done to the character.
Berserk | Master will have a chance to realize double damage in normal attack.
Magic  | Increases magical damage of the Herbalist and Explorer class.
Recover| Increases HP recovery rate of the master.
Meditation | Increases SP recovery rate of the master.

Learning skills also have chances of failure:

1. First skill book, success rate to learn = 100%
2. Second skill book, success rate to learn = 60%
3. Third skill book, success rate to learn = 30%

> **Note:**

> - Maximum of 3 skills can be taught to a pet.
> - You cannot unlearn Pet skills, except if they get overwritten by another skill.

