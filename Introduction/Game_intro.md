Game Introduction
===================
![enter image description here](http://puu.sh/sZrnk/da4d51119a.jpg)

Pirate King Online is a fully 3D designed multiplayer online game based on 5000 years of background history with Piracy as its central theme. It is comical in nature and has humorous looking characters and creatures. The games scenery is splashed with bright and beautiful colors everywhere. The exaggerated movements and actions of characters as well as objects, topped with an atmosphere that is very relaxed, makes the game consistently entertaining and always interesting and fresh for newbies and veteran players alike.


PKO features itself in the following aspects:
**High Quality 3D Cartoon-styled Design** 
**Impressive Background History and Intricate Story Quests** 
**Distinguished Characters with a lot of Professional Options** 
**Numerous Cities and Splendid Scenery** 
**Revolutionary Ship Building and Sea Battle System** 
**Myriad of Secret Searching and Treasure Hunting**
**Countless In-game Quests and GM held Events**

Yet there is still a lot to come in the following versions, namely:
**Constantly Developing New Seas and Continents** 
**More Free and Gorgeous Sea Gaming Activities** 
**New Complicated Player Interaction Systems**
**New offline stall feature**


This is just a small taste of the features available in PKO and we are constantly improving and updating.

So, if you are one who is up to the challenge of challenging the best and most ruthless pirates on the seas, if you are one who enjoys the freedom of exploration, or who desires to live a life of fantasy and danger and has an adventurous spirit, then you can't miss PKO!
