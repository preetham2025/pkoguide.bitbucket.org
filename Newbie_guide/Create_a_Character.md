Create a character
===================


1.Click the PKO game icon, and open the game interface. After the client update has finished, click "START" to enter the game.
> **Note:**
> - Please register an account to play the game
> - The client can be downloaded [here](https://pirateking.online/index.php?act=downloads).


![enter image description here](http://puu.sh/sXvmJ/08610a1353.jpg)

2.Wait for a moment and you'll enter the interface. Input your registered game ID and password to login.

![enter image description here](http://puu.sh/sXwuA/983894b7ba.jpg)

3. Enter a security code for protecting your account.

![enter image description here](http://puu.sh/sXwzw/5b44e7280b.jpg)

4.The interface for creating a character depicts the four characters at a sunny beach. Choose your favorite one and you will see the character window pop up. Fill in the character's name of your choice, and choose the Hair Style and Face, click Accept to finish the setting. You have 3 options for the character's birth place: the Argent City, Shaitan City or Icicle Castle.

![enter image description here](http://puu.sh/sXwEm/2506b0db44.jpg)

5.Give your character a name and style the hair and face.

![enter image description here](http://puu.sh/sXwGS/f13244476b.jpg)

6.Choose the birthplace for the character from the three maps available: Shaitan City, Icicle Castle, Argent City.

![enter image description here](http://puu.sh/sXwLy/3004ea5d6f.jpg)


7. Login first, as a pop up window in the beginning will remind you.

![enter image description here](http://puu.sh/sXwOt/4849d65ffd.jpg)


Congratulations! You have successfully created a character for yourself! Now you can experience the many adventures in the TOP world.


