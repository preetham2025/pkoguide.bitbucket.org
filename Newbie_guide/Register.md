Register for an Account
===================

1.Click on Login / Register Account on Pirate King Online home page. On clicking, a Login /Sign Up modal will appear.
![enter image description here](http://puu.sh/sXBt5/a2d875aa0a.png)
2. Click on Sign in with Facebook and login to your Facebook account.  
![enter image description here](http://puu.sh/sXBwM/ca35b1a18f.png)
3.Once registered you will be welcomed with a "Newbie Guide" Modal which will guide you further about your game account.
![enter image description here](http://puu.sh/sXBIq/4f9ea13160.png)
4.If you any doubts regarding Game account, click on the Help tab in the game account page.
 ![enter image description here](http://puu.sh/sXBO0/0652fde807.png)

That's it. After you've registered successfully, you're all ready to play Pirate King Online.

> **Note:**
> Facebook is used as a login/sign up platform to prevent spamming and malicious accounts.
