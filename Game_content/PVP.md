PvP System
===================
**Rules of a duel**
1. Level 25+ players can spend 5000 Gold to purchase a Medal of Valor from the Arena Administrator near the Argent Bar. With a Medal of Valor, the player can challenge somebody to a duel.
2. A duel begins once the duel invitation is accepted. To send an individual duel invitation, you need to right click on the target's portrait near the Argent Bar and then click the relevant option from the pop-up menu. To send a group duel invitation, right click on the group's leader near the Argent Bar and then click the relevant option from the pop-up menu.
3. Both sides of a duel will be teleported to a special area to fight.
> **Note:**
>  Players can send a group duel invitation or an individual duel invitation.
>The Medal of Valor keeps a record of your duels. Before you send a duel invitation, you'd better learn the opponent's composite strength.

**Duel records**

a) Duels: The number of duels increases by 1 every time you join a duel.

b) Kill: The number of characters you've slain. 

c) Death: The number of times you've been slain. 

d) Honor: Your Honor increases by 1 when you slay a character but decreases by 1 when you are slain. When your group wins a duel the number of wins increases by 1 and Honor  increases by 10. When you win a duel individually the number of wins increases by 1 and  your Honor increases by 3.

e) Punishment for fleeing a duel: When a player flees a duel, the duel is counted as a loss.