Mirage of 3 Cities
===================
The Mirage of 3 Cities are specifically designed for top level pirates, say Level 70 and above!
 
**Requirement to enter Mirage Cities :**
1) You must be between Level 70 and Level 89. 
2) Your reputation will be reduced by 50 each time you go in.
3) You must have the item Reality Mask to enter, one mask for a time. The masks could be obtained from the grocery NPCs of Argent, Shaitan and Icicle.
 
![id](http://puu.sh/sZELe/978b0dc9f1.jpg)   | ![Fairy](http://puu.sh/sZELz/7b665c1ce4.jpg) | ![enter image description here](http://puu.sh/sZEMi/cb44b068a7.jpg)
 
To buy a Reality Mask, you will be asked:
1) Elven Signet 
2) 10,000 coins
3) And one of the following materials :
     Teak Wood log X 10
     Bones X 10 
     Razor Teeth X 10
     Bubble Fish X 10 
     Crystal Eolith X 10
 
**Leaving in Mirage Cities**
You'll not lose Experience points when you die. You'll be spawned back to your spawn spot.
You can teleport out by using any ticket
You can leave by the exit teleportation point on the mirage cities map.
You'll be forced to leave when the time on the map is up. You'll be spawned back to your spawn spot.
 
**The 3 Entrances of Mirage Cities**
1) Shaitan Mirage (1244,3203)
2) Icicle Mirage (2718,659)
3) Thundoria Mirage (600,2106)

![enter image description here](http://puu.sh/sZENL/c3f54edf99.jpg)
 

Opening Times
Opens for three times a day at 8:00, 14:00 and 20:00 respectively, lasts for three hours a time.  You can alternatively check the Portal Schedule https://pirateking.online/index.php?act=portals
 
