World Glance
===================
The PKO World is huge, players who enter game for the first time will very likely get lost in the beginning; but don't worry, maps and guides can be found everywhere, so let's get some general knowledge together first.

Basically, the PKO world consists of seas, continents, big islands, isles as well as cities.

####The Whole World 
So far, the whole world can be divided into three parts: the Deep Blue, the Ascaron Continent and the Magic Ocean.

####The Three Parts 
The area of Deep Blue consists of three large islands -- Spring Island, Summer Island and Autumn Island. Apart from them, there're numerous islets which are rich in nature resources with their own distinctive features spotted all over the sea. Most of these lands are virgin, on which many animals and monsters live naturally. Besides animals and monsters there're some aboriginals who are equipped with rugged, primitive tools and were driven into the deep jungle by the adventurers, so they nourish great hostility toward outsiders.

![enter image description here](http://puu.sh/sZtj8/35106f7603.jpg)

The area of Ascaron consists of a large continent and some scattered isles, the most distinguished are the two military and merchant cities that are relatively highly developed technogically. However, during the process of their development, their nature resources dried out and become simplified. The situation didn't improve until rich resources were found in the Deep Blue area.

![enter image description here](http://puu.sh/sZtfN/ba30917ef9.jpg)


To the southwest of Magical Ocean lies a large continent covered by sand and desert. A highly developed manufacturing city is being built up in the center of the desert, the residents of which worship their ancient goddess--Cara.

![enter image description here](http://puu.sh/sZthB/e7b2f1a1b5.jpg)


####Cities 
Cities are where people gather and are also transfer stations of resources, equipment and information as well, and they're all heavily guarded. Adventurers may obtain the materials they need in the city and join different kinds of guilds freely, but all kinds of fighting are prohibited within the cities.

####City of Commerce - Argent City 
Argent City has been one of the most important cities of sea commerce. It is considered to be the most prosperous city in Ascaron Continent.

![enter image description here](http://puu.sh/sZtuI/0fda33d8c0.jpg)

Argent lies on the east coast of Ascaron.  Having a port of its own, people from all over the world gather here to barter their goods. Rows of houses line the east of the city facing the sea. At the harbor, Navy personnel can be seen doing their routine march. A Navy recruitment post is set up near the western gate, seeking to recruit all able-bodied men. Swordsmen and Explorers begin their training in this city, aiming to outshine one another. All of the people here share the same goal: To explore the world filled with adventures and endless treasure.

####City of Manufacturing - Shaitan City 
Located at the most southern point of Ascaron city, seperated from other cities by a vast desert area, Shaitan is a newly built city.


![enter image description here](http://puu.sh/sZtEf/8609516fc0.jpg)

There is a strong religious atmosphere in the whole city buildings. Citizens here worship the Goddess Kara. The only man who knows the secret to item forging is the blacksmith of this city. Adventurers seeking to help others come here to become Herbalists due to the strong religious and compassionate environment; it is said that their art of healing can even revive the dead.

####Military Base - Thundoria Castle  
A fortress advocating a warrior's lifestyle and strict military discipline. Maybe it's because it is the residence of Navy Officers.

![enter image description here](http://puu.sh/sZtJ3/2696c1c17f.png)

The military base for the Navy is surrounded by the Sacred Mountain and the deep Andes Forest. It is nearly under siege by tribesmen and monsters all year round. Cannons placed at strategic spots keep these invaders at bay. The harbor of the castle is some distance away in the northeast direction. Advanced weapons are being researched deep underground the fortress for their battle against the invaders. 

####Snow Covered Land - Icicle City  
Icicle City is a long and narrow city located at some vale bottom. Every part of the city is covered with snow.

![enter image description here](http://puu.sh/sZtLs/3d4721f5d7.png)

Many long and winding paths lead to the city itself. It is being cut off from the rest of the world with glacial gorges surrounding the city. The city was separated into an upper and lower section with most of it in ruins. Many undead creatures are waiting in those ruins to prey on innocent travelers. Due to this, many young men are trained to become Hunters to hunt down these monsters and keep the citizens safe.