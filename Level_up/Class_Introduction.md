Class Introduction
===================
Type | Class
-------- | ---
![SHIFT](http://puu.sh/sZxKz/5ab17579cb.jpg)  | **Swordsman**: After a few thousand years of intense training, Swordsmen have become experts at using swords and shields.
![enter image description here](http://puu.sh/sZxyE/ad4fac6a47.jpg) |**Hunter**: Hunters can survive in the toughest conditions. They are efficient with long distance attacks but are extremely weak when it comes to melee attacks.
![INSERT](http://puu.sh/sZxJ9/f8c0d94dae.jpg) | **Herbalist**: Their art originated from Ascaron. The Herbalists excel at all kinds of cures and use their gift to empower and aid others.
 ![ESC](http://puu.sh/sZxwA/a953709bdc.jpg) | **Crusader**: Wielding two single-handed swords simultaneously they are a fearsome sight. Crusaders are able to attack faster and they fare better than other classes at fending off attacks.
![ALT + E](http://puu.sh/sZxLV/0e8e63bb57.jpg) | **Champion**: In order to pursue greater strength, Champions have given up the use of a shield so they are able to wield two-handed weapons of great power. This greatly increases their offensive striking power.
![http://puu.sh/sZxMU/85bcc2ec7a.jpg](http://puu.sh/sZxMU/85bcc2ec7a.jpg) | **Sharpshooter**: Although their bows are revered for their long ranges, the fire gun does greater amount of damage to make up for its limited range.
![ALT + T](http://puu.sh/sZxOv/1fa4ffba7d.jpg) | **Cleric**: They are herbalists who concentrate solely on healing skills and excel in medical skills and support spells.
![F1 to F8](http://puu.sh/sZxPh/1595d9b8b0.jpg) | 	**Seal** **Master**: Using herbal research, they are able to curse and weaken enemies.
![CTRL + O to Q](http://puu.sh/sZxQ9/9282956f3f.jpg) | **Explorer**: They brave fierce gales and the raging seas to explore uncharted waters and make use of Energy Coral to channel their energy.
![ALT + S](http://puu.sh/sZxSL/939c2c2f37.jpg) |**Voyager**: They have been made famous by traveling across vast oceans and through uncharted waters where even brave explorers will not venture. Their passion for exploration will not be deterred by any danger that lies before them.

### Character Advancement

There are a variety of classes that each character can specialize in.
When players reach level 10, they can talk to their respective NPC to start the quest that is needed to select their first class advancement. When they reach level 40, they can specialize their class further by doing another quest for second class advancement. 


. | Class | City| NPC | Level 
-------- | ---
1  | **Swordsman** | Argent | Castle Guard Peter | 10 
2  | **Hunter** | Icicle | Icicle Swordsman - Ray	 | 10 
3  | **Herbalist** | Shaitan| High Priest C Gannon | 10 
4  | **Explorer** | Argent | Little Daniel | 10 
5  | **Crusader** | Argent | Castle Guard Peter | 40 
6  | **Champion** | Argent | Castle Guard Peter | 40
7  | **Cleric** | Shaitan | High Priest C Gannon | 40
8  | **Seal Master** | Shaitan | High Priest C Gannon | 40 
9  | **Sharpshooter** | Icicle | Icicle Swordsman - Ray | 40 
10  | **Voyager** | Little Daniel | Castle Guard Peter | 40 