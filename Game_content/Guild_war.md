Guild War
===================
The most exciting part of Guild System is Guild War! Every weekend, several exciting Guild Wars take place in every sever, and it is always full of fun and glory! Here are all the details you could learn: 

**Guild War Challenge Rules:**
1. Only guild leaders will be allowed to bid for Guild War challenge.
2. Guild members will not be able to bid but they can check out the status for the Guild War.
3. NPC Guild master Forlan at (831, 3548), Shaitan City is in charge of the Guild War bidding.
4. Victorious guild from previous challenge will be assigned as defending champion, Red Team and challenger will be assigned to the Blue Team.
5. Each server will have 3 defending champions respectively for the first, second and third ranking guild.
6. All guilds will be allowed to take part in Guild War challenge. Interested guilds will have to place a bid to vie for a place in the challenge. Guilds with the winning bid will be allowed to take part in the challenge at the stipulated timing. Gold will be deducted from guild leader upon placing a bid. If the bid fails, the gold will be refunded directly back to the guild leader.
7. Each bid will have to be raised by at least 50000 coins.
8. Guilds participating in Guild War challenge cannot be disbanded.
9. Defending champion will not be allowed to challenge another defending champion of lower ranking.
10. Registered challenging guild or defending champions will not be allowed to place another bid.
11. Participating guild will have to enter the map at the stipulated timing.
12. Guild War timing.
        First place challenge will start on every Saturday night at 7:00 PM ET (GMT-5), limit to 2 hours.
		Second place challenge will start on every Friday night at 7:00 PM ET (GMT-5), limit to 2 hours.
		Third place challenge will start on every Sunday night at 7:00 PM ET (GMT-5), limit to 2 hours.
		First place challenge bids will end on every Friday night at 10:00 PM ET (GMT-5).
		Second place challenge bids will end on every Thursday night at 10:00 PM ET (GMT-5).
		Third place challenge bids will end on every Friday night at 10:00 PM ET (GMT-5).
13. Defending champions will retain their position on the ranking list if they remained undefeated. However, if defending guild is defeated by challenging guild, they will automatically be placed as the next challenger for next week Guild War challenge.
14. If the battle end in a draw, the defending champion will be the victorious guild.
15. AWARD: Victorious guild leader will obtain 80% of the gold placed as bid.
NOTE:To avoid vicious pricing of guild bidding,the winning guild will only receive 35% gold if the bid price is over than 100M.
16. For a new server, the top 3 guild will be selected as stated in the following:
a) To register as a first placing guild in a new server requires 5000000 coins.
b) To register as a second placing guild in a new server requires 3000000 coins.
c) To register as a third placing guild in a new server requires 1000000 coins.
d) No gold will be refunded once registered as a defending champion.
e) Registered guild will not be able to register for another place in the ranking list.

**Guild War Map Access:**
Players can enter the map of Garden of Edel through the portal which will appear in Magical Ocean at (832, 3761).
Upon entering Garden of Edel, players will be automatically sorted into red team and blue team according to the guild they belonged to. There will be a limit on the maximum players in Garden of Edel. Each team can have up to 80 participating members.

Map will close one minute after one side wins.

**Victory Condition:**
Destroy opponents base in 2 hours or challenge will end in a draw. If player disconnected or logged off in Garden of Edel, he or she will be sent back to the last recorded Spawn point. There will not be any death penalty in Garden of Edel.

**Building Introduction:**
Name | Description
-------- | ---
**Base**|Each side will have a main structure surrounded by many high level cannons. It will have a base physical resist value of 80 with high Max HP and Attack. Once destroyed, it will result in a guild defeat. Base can only be attacked by land. It also has the ability to reveal invisible unit and can be repaired when damaged.
**Granary**|Each side will have a granary surrounded by low level cannons. Once destroyed, respective base will have a 50 reduction of defense value. It also has the ability to reveal invisible unit and can be repaired when damaged.
**Ammo Warehouse**|Each side will have a ammunition warehouse surrounded by low level cannons. Once destroyed, respective base will have a 50 reduction of attack value. It also has the ability to reveal invisible unit and can be repaired when damaged.
**CannonTower**|Defensive structures which both team possess. It will be unable to move about or respawn upon destruction. Cannon Tower will only attack enemy targets. It also has the ability to reveal invisible unit and can be repaired when damaged.

**Map Resources:**
Name | Description
-------- | ---
**Werewolf Archer**|Kill to obtain special potions.
**Pirate Leader**|Kill to obtain Blessed Potion.
**Wood**|Wood collected can be used to repair damaged cannon tower or other building structures (Requires Repair as one of the life skill. Skill book can be purchased from Argent Grocer).
**Trader**|Sells high quality wood for repair of ships’ durability. Only 200 pieces will be sold every 15 minutes.

**Map Miscellaneous Items:**
Name | Description
-------- | ---
Bull Potion	|Max HP +1000
Battle Potion|Attack +150
Berserk Potion|Attack Speed +140
Energy Potion|Stamina +30
Harden Potion|Defense +150
Accurate Potion|Hit Rate +30
Blessed Potion|Player becomes invulnerable for 15 seconds when used.

> **Note:**
>  All potions will be removed by System upon leaving the Guild War Map.

The map:

**Guild War Map** 

![enter image description here](http://puu.sh/sZKXK/940ce13ea6.jpg)




Screenshots:

![enter image description here](http://puu.sh/sZL28/8c1d5d8a2c.jpg) | ![Description](http://puu.sh/sZL3b/d30dd42493.jpg)
-------- | ---
Base|Granary



![enter image description here](http://puu.sh/sZL4Q/9edddd3a5c.jpg) | ![Description](http://puu.sh/sZL5R/df01669ad3.jpg)
-------- | ---
Volcano|Iron Trees

![enter image description here](http://puu.sh/sZL7E/00781c6085.jpg) | ![Description](http://puu.sh/sZL89/1f2a614d05.jpg)
-------- | ---
Guard Tower|Guild War!


###Special Privileges

To become the winners of Guild War not only means reputation and glory, but real benefits as well-- that is Special Commerce Privileges!
 
**Special Commerce Privileges**
1.	Guild of the first, second and third place will have privilege to deal in special products for commercial trade.
2.	Dealing in special products will reap in huge amount of profits.
3.	1st place guild will have privilege to purchase products from Shaitan City Arms Dealer for commerce trade.
4.	2nd place guild will have privilege to purchase products from Shaitan City Rare Material Supplier for commerce trade.
5.	3rd place guild will have privilege to purchase products from Shaitan City Rare Metal Supplier for commerce trade.
6.	Players will be able to sell these products at a high price to the Trader in Thundoria Navy Harbor.
7.	Please notice that other traders from other ports will not collect these special products.
 