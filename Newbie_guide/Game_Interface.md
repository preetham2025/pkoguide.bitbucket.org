Game Interface
===================
1. Character State
 This box shows the character's current state, including portrait, HP, MP and EXP.
 ![enter image description here](http://puu.sh/sXxX7/c2a08f088d.jpg)
 
2. Navigation Map
The navigation map shows areas around the player. The center of the map shows where the player is currently located. To zoom the map out, simply press the "+" key. Pressing ↓ key unfolds the map radar. This allows players to search for a spot simply by entering the coordinates.
![enter image description here](http://puu.sh/sXy0d/507be8ea6b.jpg)

3. Item Bar
![enter image description here](http://puu.sh/sXy2n/6823593efa.jpg)


4. Communications
![](http://puu.sh/sXyuJ/d6bd28b442.jpg)

5. Skill
![enter image description here](http://puu.sh/sXyxy/7504638cdb.jpg)

6. System Option
![enter image description here](http://puu.sh/sXyBd/d101885a0a.png)


7. Shortcut Keys Bar
There are 2 rows of shortcut keys. To switch between the 2 rows, simply left click on the buttons to the right of the shortcut keys bar. Each row consists of 8 shortcut keys at most. You can drag items or skills to these slots so you can use them right away simply by pressing the related key. If you want to delete a shortcut key, just right click on the icon or move the icon out of the shortcut keys bar.
![enter image description here](http://puu.sh/sXyEN/04f0d6ac8e.png)


8. Action/Emoticon/Chat
 When clicking on an action or emoticon, the character will perform the action or broadcast the emoticon.Clicking on the chat button unfolds the chat box, where you can add friends, send a party invite, chat, etc.
![enter image description here](http://puu.sh/sXyKn/15e26333be.jpg)
